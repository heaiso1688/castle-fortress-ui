# fortress-basic-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### 组件  
#### fortress-region 使用说明（可参照 组件示例-编辑页面）
<fortress-region
          ref="fortressRegion"
          @ftregion="regionHandler"
        ></fortress-region>
初始化地区数据：        
    新增时： this.$refs.fortressRegion.init([]);  
    修改时：
    1 读取省/市/区 编码 放入一个数组 比如region
    let region=[]
    region.push(this.form.province);
    region.push(this.form.city);
    region.push(this.form.area);
    2 调用组件 this.$refs.fortressRegion.init(region);
地区数据选择后回显：
//regions 选中的节点的id集合，清空的话为空数组
//regionsInfo 选中的节点集合（包含id、name),清空的话为空数组
regionHandler(regions,regionsInfo) {
      console.log("regionHandler", regions);
},    

取消编辑/提交保存时 需要调用地区组件的隐藏方法
this.$refs.fortressRegion.hide();   
#### fortress-map 高德地图组件 使用说明（可参照 门店管理页面）
<fortress-map @ftmap="changeLogLat"></fortress-map>

//获取选择的经纬度 和 地址
    changeLogLat(position, address) {
      this.innerVisible = false;
      console.log(position, address);
      if (position) {
        this.form.longitude = position[0];
        this.form.latitude = position[1];
        this.form.longitude_latitude =
          this.form.longitude + "," + this.form.latitude;
      }
    },

#### fortress-upload 上传组件 使用说明（可参照 组件示例-编辑页面）
    <fortress-upload
          :size="7"
          type="image"
          name="images"
          :formData="form"
        ></fortress-upload>
 属性说明：
 type：上传组件类型,字符串类型，必填。支持 "image"(图片上传)；"file"(文件上传)；"video"（视频类型上传）；"videoShow"（视频类型展示）
 size：上传文字大小，单位M，数字类型，可选。
 name：该组件绑定的表单字段名，必填（可参照 组件示例-编辑页面）
 formData：name对应的字段所在的表单对象，必填（可参照 组件示例-编辑页面）
 multiple：是否支持多图片，默认为false（单图片），当类型为"image"时该属性生效。注：多图片时返回类型为数组类型
 videoSrc：视频地址，当类型为"videoShow"时该属性生效（可参照 组件示例-详情页面、列表页面）

#### fortress-industry 行业选择器 使用说明（可参照 组件示例-编辑页面）
    <fortress-industry
            :checkId="id" 
            @check="check" 
            placeholder="请选择职位" 
            title="请选择职位类别" 
            width="60%" 
        ></fortress-industry>
 属性说明：
 checkId：组件初始化要显示的职位的id
 check：返回选中职位的数据
 placeholder：无选中职位时输入框显示的文字
 title：弹出框的标题
 width：弹出框的宽度

 #### 部署时涉及子文件夹时需确认以下文件中的路径是否正确：
 1 vue.config.js
 2 views\form\formconfig-preview.vue
 3 views\form\formconfig.vue
 4 views\form\formdata-edit.vue
