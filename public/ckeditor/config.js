/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */
CKEDITOR.editorConfig = function(config) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//关闭他的过滤器
	config.allowedContent = true
	// 服务器端上传图片接口URL
	config.filebrowserImageUploadUrl = window.$conf.commonConf.baseUrl + "/system/oss/ckupload"
	// //工具栏
	// config.toolbar = [
	// 	{ name: 'document', items: ['Source', '-', 'NewPage', 'Preview', 'TextColor', '-', 'Templates'] },
	// 	{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
	// 	{ name: 'basicstyles', items: ['Bold', 'Italic'] }
	// ];

	config.toolbar = [
		// { name: "document", items: ["Source ", "- ", "Save ", "NewPage ", "Preview", "-", "Templates"] },
		{ name: "clipboard", items: ["PasteText", "PasteFromword", "- "] },
		{ name: "editing", items: ["Find", "Replace", "- "] },
		// { name: "forms", items: ["Form", "TextField", "Textarea", "Select", "HiddenField"] },
		{
			name: "paragraph",
			items: [
				"NumberedList",
				"BulletedList",
				"-",
				"Outdent",
				"Indent",
				"-",
				"Blockquote",
				"-",
				"JustifyLeft",
				"JustifyCenter",
				"JustifyRight",
				"JustifyBlock",
				"-",
				"BidiLtr",
				"BidiRtl",
			],
		},
		{ name: "links", items: ["Link", "Unlink", "Anchor"] },
		{
			name: "insert",
			items: ["addpic" /* Image */, , "Flash", "Table", "HorizontalRule", "Smiley", "SpecialChar", "PageBreak", "Iframe"],
		},
		"/",
		{
			name: "basicstyles",
			items: ["Bold", "Italic", "Underline", "Strike ", "Subscript", "Superscript ", " - ", "CopyFormatting ", "RemoveFonmat"],
		},
		{ name: "styles", items: ["Format", "Font", "FontSize"] },
		{ name: "colors", items: ["TextColor", "BGColor"] },
		{ name: "tools", items: ["Maximize", "ShowBlocks"] },
	]
	config.extraPlugins = "addpic"
}
