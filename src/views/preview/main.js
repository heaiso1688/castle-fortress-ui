import Vue from "vue"
import "element-ui/lib/theme-chalk/index.css"
import * as commonConf from "@/common/config.js"
import Element from "element-ui"
import { Message } from "element-ui"
import "@/element-variables.scss"
import { loadScriptQueue } from "@/components/fortress-draggable/utils/loadScript"
import axios from "axios"
import Tinymce from "@/components/fortress-draggable/tinymce/index.vue"
//语雀视频组件
import VAuditer from "@/components/auditer"
import EleForm from "vue-ele-form"
import EleFormGallery from "vue-ele-form-gallery" //图片、视频展示组件
import EleFormVideoUploader from "vue-ele-form-video-uploader" //视频上传
import EleFormImageUploader from "vue-ele-form-image-uploader" //图片上传
import EleFormUploadFile from "vue-ele-form-upload-file"
import FortressDateTimeStat from "@/components/fortress-date-time-stat/index.vue"
import FortressDateTimeStatDay from "@/components/fortress-date-time-stat-day/index.vue"
// 注册 gallery 组件 图片、视频展示组件
Vue.component("gallery", EleFormGallery)
// 注册 ele-form
Vue.use(EleForm, {
	// 可以在这里设置全局的 gallery 属性
	// 属性参考下方 #attrs
	gallery: {
		size: 150,
	},
})
// 注册 video-uploader 组件
Vue.component("video-uploader", EleFormVideoUploader)
// 注册 image-uploader 组件
Vue.component("image-uploader", EleFormImageUploader)
// 注册 upload-file 组件
Vue.component("upload-file", EleFormUploadFile)
import FortressUpload from "@/components/fortress-upload"
//框架封装组件
import FortressEditor from "@/components/ckeditor"
Vue.use(Element)
Vue.use(FortressEditor)
Vue.use(VAuditer)
Vue.use(FortressUpload)
Vue.component("FortressDateTimeStat", FortressDateTimeStat)
Vue.component("FortressDateTimeStatDay", FortressDateTimeStatDay)
//业务组件
import FortressRegion from "@/components/fortress-region"
Vue.use(FortressRegion)

Vue.component("tinymce", Tinymce)
Vue.prototype.$msg = Message
window.$conf = {
	commonConf,
}
Vue.prototype.$axios = axios

const $previewApp = document.getElementById("previewApp")
const childAttrs = {
	file: "",
	dialog: ' width="600px" class="dialog-width" v-if="visible" :visible.sync="visible" :modal-append-to-body="false" ',
}

window.addEventListener("message", init, false)

function buildLinks(links) {
	let strs = ""
	links.forEach(url => {
		strs += `<link href="${url}" rel="stylesheet">`
	})
	return strs
}

function init(event) {
	if (event.data.type === "refreshFrame") {
		const code = event.data.data
		const attrs = childAttrs[code.generateConf.type]
		let links = ""

		if (Array.isArray(code.links) && code.links.length > 0) {
			links = buildLinks(code.links)
		}

		$previewApp.innerHTML = `${links}<style>${code.css}</style><div id="app"></div>`

		if (Array.isArray(code.scripts) && code.scripts.length > 0) {
			loadScriptQueue(code.scripts, () => {
				newVue(attrs, code.js, code.html)
			})
		} else {
			newVue(attrs, code.js, code.html)
		}
	}
}

function newVue(attrs, main, html) {
	main = eval(`(${main})`)
	main.template = `<div>${html}</div>`
	new Vue({
		components: {
			child: main,
		},
		data() {
			return {
				visible: true,
			}
		},
		template: `<div><child ${attrs}/></div>`,
	}).$mount("#app")
}
