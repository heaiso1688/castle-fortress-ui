//通用混淆方法
export const ct = {
	methods: {
		//资源选择器选中数据
		resourceChoosed(obj, model, multiple) {
			if (obj.ossPlatform == 1) {
				obj.resourceUrl = this.$store.state.prefixUrl + obj.resourceUrl
			}
			let d = model.split(".")
			if (d.length == 1) {
				if (multiple) {
					if (!this.$data[model]) {
						this.$data[model] = []
					}
					this.$nextTick(() => {
						this.$data[model].push(obj.resourceUrl)
					})
				} else {
					this.$data[model] = obj.resourceUrl
				}
			} else if (d.length == 2) {
				if (multiple) {
					if (!this.$data[d[0]][d[1]]) {
						this.$data[d[0]][d[1]] = []
					}
					this.$nextTick(() => {
						this.$data[d[0]][d[1]].push(obj.resourceUrl)
					})
				} else {
					this.$data[d[0]][d[1]] = obj.resourceUrl
				}
			} else if (d.length == 3) {
				if (multiple) {
					if (!this.$data[d[0]][d[1]][d[2]]) {
						this.$data[d[0]][d[1]][d[2]] = []
					}
					this.$nextTick(() => {
						this.$data[d[0]][d[1]][d[2]].push(obj.resourceUrl)
					})
				} else {
					this.$data[d[0]][d[1]][d[2]] = obj.resourceUrl
				}
			}
		},
	},
}
