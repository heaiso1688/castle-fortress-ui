import request from "@/common/request"

//配送方式分页展示
export const deliverymethodPage = (queryForm) => {
    return request("get", "/goods/deliveryMethod/page", queryForm);
}
//配送方式列表展示
export const deliverymethodList = (queryForm) => {
    return request("get", "/goods/deliveryMethod/list", queryForm);
}
//配送方式详情
export const deliverymethodInfo = (id) => {
    return request('get', '/goods/deliveryMethod/info', {
        id: id
    })
}
//配送方式信息保存
export const deliverymethodSave = (data) => {
    return request('post', '/goods/deliveryMethod/save', data)
}
//配送方式信息修改
export const deliverymethodEdit = (data) => {
    return request('post', '/goods/deliveryMethod/edit', data)
}
//配送方式信息删除
export const deliverymethodDel = (id) => {
    return request('post', '/goods/deliveryMethod/delete?id=' + id)
}
//配送方式批量删除
export const deliverymethodDelBatch = (ids) => {
    return request('post', '/goods/deliveryMethod/deleteBatch', ids)
}
//配送方式动态表头导出
export const deliverymethodDynamicExport = (data) => {
    return request("eptpost", '/goods/deliveryMethod/exportDynamic', data)
}
