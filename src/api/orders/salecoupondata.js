import request from "@/common/request"

//优惠券领取记录分页展示
export const salecoupondataPage = (queryForm) => {
    return request("get", "/orders/saleCouponData/page", queryForm);
}
//优惠券领取记录列表展示
export const salecoupondataList = (queryForm) => {
    return request("get", "/orders/saleCouponData/list", queryForm);
}
//优惠券领取记录详情
export const salecoupondataInfo = (id) => {
    return request('get', '/orders/saleCouponData/info', {
        id: id
    })
}
//优惠券领取记录信息保存
export const salecoupondataSave = (data) => {
    return request('post', '/orders/saleCouponData/save', data)
}
//优惠券领取记录信息修改
export const salecoupondataEdit = (data) => {
    return request('post', '/orders/saleCouponData/edit', data)
}
//优惠券领取记录信息删除
export const salecoupondataDel = (id) => {
    return request('post', '/orders/saleCouponData/delete?id=' + id)
}
//优惠券领取记录批量删除
export const salecoupondataDelBatch = (ids) => {
    return request('post', '/orders/saleCouponData/deleteBatch', ids)
}
//优惠券领取记录动态表头导出
export const salecoupondataDynamicExport = (data) => {
    return request("eptpost", '/orders/saleCouponData/exportDynamic', data)
}
