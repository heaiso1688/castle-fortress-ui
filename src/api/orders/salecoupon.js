import request from "@/common/request"

//优惠券分页展示
export const salecouponPage = (queryForm) => {
    return request("get", "/orders/saleCoupon/page", queryForm);
}
//优惠券列表展示
export const salecouponList = (queryForm) => {
    return request("get", "/orders/saleCoupon/list", queryForm);
}
//优惠券详情
export const salecouponInfo = (id) => {
    return request('get', '/orders/saleCoupon/info', {
        id: id
    })
}
//优惠券信息保存
export const salecouponSave = (data) => {
    return request('post', '/orders/saleCoupon/save', data)
}
//优惠券信息修改
export const salecouponEdit = (data) => {
    return request('post', '/orders/saleCoupon/edit', data)
}
//优惠券信息删除
export const salecouponDel = (id) => {
    return request('post', '/orders/saleCoupon/delete?id=' + id)
}
//优惠券批量删除
export const salecouponDelBatch = (ids) => {
    return request('post', '/orders/saleCoupon/deleteBatch', ids)
}
//优惠券动态表头导出
export const salecouponDynamicExport = (data) => {
    return request("eptpost", '/orders/saleCoupon/exportDynamic', data)
}

export const salecouponSend = (data) => {
    return request('post', '/orders/saleCoupon/send', data)
}