import request from "@/common/request"

//会员积分流水分页展示
export const memberpointsserialPage = (queryForm) => {
    return request("get", "/member/memberPointsSerial/page", queryForm);
}
//会员积分流水列表展示
export const memberpointsserialList = (queryForm) => {
    return request("get", "/member/memberPointsSerial/list", queryForm);
}
//会员积分流水详情
export const memberpointsserialInfo = (id) => {
    return request('get', '/member/memberPointsSerial/info', {
        id: id
    })
}
//会员积分流水信息保存
export const memberpointsserialSave = (data) => {
    return request('post', '/member/memberPointsSerial/save', data)
}
//会员积分流水信息修改
export const memberpointsserialEdit = (data) => {
    return request('post', '/member/memberPointsSerial/edit', data)
}
//会员积分流水信息删除
export const memberpointsserialDel = (id) => {
    return request('post', '/member/memberPointsSerial/delete?id=' + id)
}
//会员积分流水批量删除
export const memberpointsserialDelBatch = (ids) => {
    return request('post', '/member/memberPointsSerial/deleteBatch', ids)
}
//会员积分流水动态表头导出
export const memberpointsserialDynamicExport = (data) => {
    return request("eptpost", '/member/memberPointsSerial/exportDynamic', data)
}
