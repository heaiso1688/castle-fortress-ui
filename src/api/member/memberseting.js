import request from "@/common/request"

//会员设置表分页展示
export const membersetingPage = (queryForm) => {
    return request("get", "/member/memberSeting/page", queryForm);
}
//会员设置表列表展示
export const membersetingList = (queryForm) => {
    return request("get", "/member/memberSeting/list", queryForm);
}
//会员设置表详情
export const membersetingInfo = (id) => {
    return request('get', '/member/memberSeting/info', {
        id: id
    })
}
//会员设置表信息保存
export const membersetingSave = (data) => {
    return request('post', '/member/memberSeting/save', data)
}
//会员设置表信息修改
export const membersetingEdit = (data) => {
    return request('post', '/member/memberSeting/edit', data)
}
//会员设置表信息删除
export const membersetingDel = (id) => {
    return request('post', '/member/memberSeting/delete?id=' + id)
}
//会员设置表批量删除
export const membersetingDelBatch = (ids) => {
    return request('post', '/member/memberSeting/deleteBatch', ids)
}
//会员设置表动态表头导出
export const membersetingDynamicExport = (data) => {
    return request("eptpost", '/member/memberSeting/exportDynamic', data)
}
