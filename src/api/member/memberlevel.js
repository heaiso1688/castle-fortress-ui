import request from "@/common/request"

//会员等级表分页展示
export const memberlevelPage = (queryForm) => {
    return request("get", "/member/memberLevel/page", queryForm);
}
//会员等级表列表展示
export const memberlevelList = (queryForm) => {
    return request("get", "/member/memberLevel/list", queryForm);
}
//会员等级表详情
export const memberlevelInfo = (id) => {
    return request('get', '/member/memberLevel/info', {
        id: id
    })
}
//会员等级表信息保存
export const memberlevelSave = (data) => {
    return request('post', '/member/memberLevel/save', data)
}
//会员等级表信息修改
export const memberlevelEdit = (data) => {
    return request('post', '/member/memberLevel/edit', data)
}
//会员等级表信息删除
export const memberlevelDel = (id) => {
    return request('post', '/member/memberLevel/delete?id=' + id)
}
//会员等级表批量删除
export const memberlevelDelBatch = (ids) => {
    return request('post', '/member/memberLevel/deleteBatch', ids)
}
//会员等级表动态表头导出
export const memberlevelDynamicExport = (data) => {
    return request("eptpost", '/member/memberLevel/exportDynamic', data)
}
