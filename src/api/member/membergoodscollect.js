import request from "@/common/request"

//会员商品收藏表分页展示
export const membergoodscollectPage = (queryForm) => {
    return request("get", "/member/memberGoodsCollect/page", queryForm);
}
//会员商品收藏表列表展示
export const membergoodscollectList = (queryForm) => {
    return request("get", "/member/memberGoodsCollect/list", queryForm);
}
//会员商品收藏表详情
export const membergoodscollectInfo = (id) => {
    return request('get', '/member/memberGoodsCollect/info', {
        id: id
    })
}
//会员商品收藏表信息保存
export const membergoodscollectSave = (data) => {
    return request('post', '/member/memberGoodsCollect/save', data)
}
//会员商品收藏表信息修改
export const membergoodscollectEdit = (data) => {
    return request('post', '/member/memberGoodsCollect/edit', data)
}
//会员商品收藏表信息删除
export const membergoodscollectDel = (id) => {
    return request('post', '/member/memberGoodsCollect/delete?id=' + id)
}
//会员商品收藏表批量删除
export const membergoodscollectDelBatch = (ids) => {
    return request('post', '/member/memberGoodsCollect/deleteBatch', ids)
}
//会员商品收藏表动态表头导出
export const membergoodscollectDynamicExport = (data) => {
    return request("eptpost", '/member/memberGoodsCollect/exportDynamic', data)
}
