import request from "@/common/request"

//会员分组表分页展示
export const membergroupPage = (queryForm) => {
    return request("get", "/member/memberGroup/page", queryForm);
}
//会员分组表列表展示
export const membergroupList = (queryForm) => {
    return request("get", "/member/memberGroup/list", queryForm);
}
//会员分组表详情
export const membergroupInfo = (id) => {
    return request('get', '/member/memberGroup/info', {
        id: id
    })
}
//会员分组表信息保存
export const membergroupSave = (data) => {
    return request('post', '/member/memberGroup/save', data)
}
//会员分组表信息修改
export const membergroupEdit = (data) => {
    return request('post', '/member/memberGroup/edit', data)
}
//会员分组表信息删除
export const membergroupDel = (id) => {
    return request('post', '/member/memberGroup/delete?id=' + id)
}
//会员分组表批量删除
export const membergroupDelBatch = (ids) => {
    return request('post', '/member/memberGroup/deleteBatch', ids)
}
//会员分组表动态表头导出
export const membergroupDynamicExport = (data) => {
    return request("eptpost", '/member/memberGroup/exportDynamic', data)
}
