import request from "@/common/request"

//会员银行卡表分页展示
export const memberbankcardPage = (queryForm) => {
    return request("get", "/member/memberBankCard/page", queryForm);
}
//会员银行卡表列表展示
export const memberbankcardList = (queryForm) => {
    return request("get", "/member/memberBankCard/list", queryForm);
}
//会员银行卡表详情
export const memberbankcardInfo = (id) => {
    return request('get', '/member/memberBankCard/info', {
        id: id
    })
}
//会员银行卡表信息保存
export const memberbankcardSave = (data) => {
    return request('post', '/member/memberBankCard/save', data)
}
//会员银行卡表信息修改
export const memberbankcardEdit = (data) => {
    return request('post', '/member/memberBankCard/edit', data)
}
//会员银行卡表信息删除
export const memberbankcardDel = (id) => {
    return request('post', '/member/memberBankCard/delete?id=' + id)
}
//会员银行卡表批量删除
export const memberbankcardDelBatch = (ids) => {
    return request('post', '/member/memberBankCard/deleteBatch', ids)
}
//会员银行卡表动态表头导出
export const memberbankcardDynamicExport = (data) => {
    return request("eptpost", '/member/memberBankCard/exportDynamic', data)
}
