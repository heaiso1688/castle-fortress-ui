import request from "@/common/request"

//会员收货地址表分页展示
export const memberaddressPage = (queryForm) => {
    return request("get", "/member/memberAddress/page", queryForm);
}
//会员收货地址表列表展示
export const memberaddressList = (queryForm) => {
    return request("get", "/member/memberAddress/list", queryForm);
}
//会员收货地址表详情
export const memberaddressInfo = (id) => {
    return request('get', '/member/memberAddress/info', {
        id: id
    })
}
//会员收货地址表信息保存
export const memberaddressSave = (data) => {
    return request('post', '/member/memberAddress/save', data)
}
//会员收货地址表信息修改
export const memberaddressEdit = (data) => {
    return request('post', '/member/memberAddress/edit', data)
}
//会员收货地址表信息删除
export const memberaddressDel = (id) => {
    return request('post', '/member/memberAddress/delete?id=' + id)
}
//会员收货地址表批量删除
export const memberaddressDelBatch = (ids) => {
    return request('post', '/member/memberAddress/deleteBatch', ids)
}
//会员收货地址表动态表头导出
export const memberaddressDynamicExport = (data) => {
    return request("eptpost", '/member/memberAddress/exportDynamic', data)
}
