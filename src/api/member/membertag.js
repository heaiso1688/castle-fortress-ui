import request from "@/common/request"

//会员标签表分页展示
export const membertagPage = (queryForm) => {
    return request("get", "/member/memberTag/page", queryForm);
}
//会员标签表列表展示
export const membertagList = (queryForm) => {
    return request("get", "/member/memberTag/list", queryForm);
}
//会员标签表详情
export const membertagInfo = (id) => {
    return request('get', '/member/memberTag/info', {
        id: id
    })
}
//会员标签表信息保存
export const membertagSave = (data) => {
    return request('post', '/member/memberTag/save', data)
}
//会员标签表信息修改
export const membertagEdit = (data) => {
    return request('post', '/member/memberTag/edit', data)
}
//会员标签表信息删除
export const membertagDel = (id) => {
    return request('post', '/member/memberTag/delete?id=' + id)
}
//会员标签表批量删除
export const membertagDelBatch = (ids) => {
    return request('post', '/member/memberTag/deleteBatch', ids)
}
//会员标签表动态表头导出
export const membertagDynamicExport = (data) => {
    return request("eptpost", '/member/memberTag/exportDynamic', data)
}
