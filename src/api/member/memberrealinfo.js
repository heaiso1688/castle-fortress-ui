import request from "@/common/request"

//会员真实信息表分页展示
export const memberrealinfoPage = (queryForm) => {
    return request("get", "/member/memberRealinfo/page", queryForm);
}
//会员真实信息表列表展示
export const memberrealinfoList = (queryForm) => {
    return request("get", "/member/memberRealinfo/list", queryForm);
}
//会员真实信息表详情
export const memberrealinfoInfo = (id) => {
    return request('get', '/member/memberRealinfo/info', {
        id: id
    })
}
//会员真实信息表信息保存
export const memberrealinfoSave = (data) => {
    return request('post', '/member/memberRealinfo/save', data)
}
//会员真实信息表信息修改
export const memberrealinfoEdit = (data) => {
    return request('post', '/member/memberRealinfo/edit', data)
}
//会员真实信息表信息删除
export const memberrealinfoDel = (id) => {
    return request('post', '/member/memberRealinfo/delete?id=' + id)
}
//会员真实信息表批量删除
export const memberrealinfoDelBatch = (ids) => {
    return request('post', '/member/memberRealinfo/deleteBatch', ids)
}
//会员真实信息表动态表头导出
export const memberrealinfoDynamicExport = (data) => {
    return request("eptpost", '/member/memberRealinfo/exportDynamic', data)
}
