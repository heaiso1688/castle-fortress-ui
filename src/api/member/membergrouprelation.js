import request from "@/common/request"

//会员分组关联关系表分页展示
export const membergrouprelationPage = (queryForm) => {
    return request("get", "/member/memberGroupRelation/page", queryForm);
}
//会员分组关联关系表列表展示
export const membergrouprelationList = (queryForm) => {
    return request("get", "/member/memberGroupRelation/list", queryForm);
}
//会员分组关联关系表详情
export const membergrouprelationInfo = (id) => {
    return request('get', '/member/memberGroupRelation/info', {
        id: id
    })
}
//会员分组关联关系表信息保存
export const membergrouprelationSave = (data) => {
    return request('post', '/member/memberGroupRelation/save', data)
}
//会员分组关联关系表信息修改
export const membergrouprelationEdit = (data) => {
    return request('post', '/member/memberGroupRelation/edit', data)
}
//会员分组关联关系表信息删除
export const membergrouprelationDel = (id) => {
    return request('post', '/member/memberGroupRelation/delete?id=' + id)
}
//会员分组关联关系表批量删除
export const membergrouprelationDelBatch = (ids) => {
    return request('post', '/member/memberGroupRelation/deleteBatch', ids)
}
//会员分组关联关系表动态表头导出
export const membergrouprelationDynamicExport = (data) => {
    return request("eptpost", '/member/memberGroupRelation/exportDynamic', data)
}
