import request from "@/common/request"

//微信会员表分页展示
export const memberwxPage = (queryForm) => {
    return request("get", "/member/memberWx/page", queryForm);
}
//微信会员表列表展示
export const memberwxList = (queryForm) => {
    return request("get", "/member/memberWx/list", queryForm);
}
//微信会员表详情
export const memberwxInfo = (id) => {
    return request('get', '/member/memberWx/info', {
        id: id
    })
}
//微信会员表信息保存
export const memberwxSave = (data) => {
    return request('post', '/member/memberWx/save', data)
}
//微信会员表信息修改
export const memberwxEdit = (data) => {
    return request('post', '/member/memberWx/edit', data)
}
//微信会员表信息删除
export const memberwxDel = (id) => {
    return request('post', '/member/memberWx/delete?id=' + id)
}
//微信会员表批量删除
export const memberwxDelBatch = (ids) => {
    return request('post', '/member/memberWx/deleteBatch', ids)
}
//微信会员表动态表头导出
export const memberwxDynamicExport = (data) => {
    return request("eptpost", '/member/memberWx/exportDynamic', data)
}
