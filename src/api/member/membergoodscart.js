import request from "@/common/request"

//会员商品购物车分页展示
export const membergoodscartPage = (queryForm) => {
    return request("get", "/member/memberGoodsCart/page", queryForm);
}
//会员商品购物车列表展示
export const membergoodscartList = (queryForm) => {
    return request("get", "/member/memberGoodsCart/list", queryForm);
}
//会员商品购物车详情
export const membergoodscartInfo = (id) => {
    return request('get', '/member/memberGoodsCart/info', {
        id: id
    })
}
//会员商品购物车信息保存
export const membergoodscartSave = (data) => {
    return request('post', '/member/memberGoodsCart/save', data)
}
//会员商品购物车信息修改
export const membergoodscartEdit = (data) => {
    return request('post', '/member/memberGoodsCart/edit', data)
}
//会员商品购物车信息删除
export const membergoodscartDel = (id) => {
    return request('post', '/member/memberGoodsCart/delete?id=' + id)
}
//会员商品购物车批量删除
export const membergoodscartDelBatch = (ids) => {
    return request('post', '/member/memberGoodsCart/deleteBatch', ids)
}
//会员商品购物车动态表头导出
export const membergoodscartDynamicExport = (data) => {
    return request("eptpost", '/member/memberGoodsCart/exportDynamic', data)
}
