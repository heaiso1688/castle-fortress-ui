import request from "@/common/request"

//会员积分表分页展示
export const memberpointsPage = (queryForm) => {
    return request("get", "/member/memberPoints/page", queryForm);
}
//会员积分表列表展示
export const memberpointsList = (queryForm) => {
    return request("get", "/member/memberPoints/list", queryForm);
}
//会员积分表详情
export const memberpointsInfo = (id) => {
    return request('get', '/member/memberPoints/info', {
        id: id
    })
}
//会员积分表信息保存
export const memberpointsSave = (data) => {
    return request('post', '/member/memberPoints/save', data)
}
//会员积分表信息修改
export const memberpointsEdit = (data) => {
    return request('post', '/member/memberPoints/edit', data)
}
//会员积分表信息删除
export const memberpointsDel = (id) => {
    return request('post', '/member/memberPoints/delete?id=' + id)
}
//会员积分表批量删除
export const memberpointsDelBatch = (ids) => {
    return request('post', '/member/memberPoints/deleteBatch', ids)
}
//会员积分表动态表头导出
export const memberpointsDynamicExport = (data) => {
    return request("eptpost", '/member/memberPoints/exportDynamic', data)
}
