import request from "@/common/request"

//会员账户流水分页展示
export const memberaccountserialPage = (queryForm) => {
    return request("get", "/member/memberAccountSerial/page", queryForm);
}
//会员账户流水列表展示
export const memberaccountserialList = (queryForm) => {
    return request("get", "/member/memberAccountSerial/list", queryForm);
}
//会员账户流水详情
export const memberaccountserialInfo = (id) => {
    return request('get', '/member/memberAccountSerial/info', {
        id: id
    })
}
//会员账户流水信息保存
export const memberaccountserialSave = (data) => {
    return request('post', '/member/memberAccountSerial/save', data)
}
//会员账户流水信息修改
export const memberaccountserialEdit = (data) => {
    return request('post', '/member/memberAccountSerial/edit', data)
}
//会员账户流水信息删除
export const memberaccountserialDel = (id) => {
    return request('post', '/member/memberAccountSerial/delete?id=' + id)
}
//会员账户流水批量删除
export const memberaccountserialDelBatch = (ids) => {
    return request('post', '/member/memberAccountSerial/deleteBatch', ids)
}
//会员账户流水动态表头导出
export const memberaccountserialDynamicExport = (data) => {
    return request("eptpost", '/member/memberAccountSerial/exportDynamic', data)
}
