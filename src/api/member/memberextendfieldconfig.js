import request from "@/common/request"

//字段生成表
export const memberextendfieldconfigGenerate = (queryForm) => {
    return request("get", "/member/memberExtendFieldConfig/generate", queryForm);
}
//用户扩展字段配置表分页展示
export const memberextendfieldconfigPage = (queryForm) => {
    return request("get", "/member/memberExtendFieldConfig/page", queryForm);
}
//用户扩展字段配置表列表展示
export const memberextendfieldconfigList = (queryForm) => {
    return request("get", "/member/memberExtendFieldConfig/list", queryForm);
}
//用户扩展字段配置表详情
export const memberextendfieldconfigInfo = (id) => {
    return request('get', '/member/memberExtendFieldConfig/info', {
        id: id
    })
}
//用户扩展字段配置表信息保存
export const memberextendfieldconfigSave = (data) => {
    return request('post', '/member/memberExtendFieldConfig/save', data)
}
//用户扩展字段配置表信息修改
export const memberextendfieldconfigEdit = (data) => {
    return request('post', '/member/memberExtendFieldConfig/edit', data)
}
//用户扩展字段配置表信息删除
export const memberextendfieldconfigDel = (id) => {
    return request('post', '/member/memberExtendFieldConfig/delete?id=' + id)
}
//用户扩展字段配置表批量删除
export const memberextendfieldconfigDelBatch = (ids) => {
    return request('post', '/member/memberExtendFieldConfig/deleteBatch', ids)
}
//用户扩展字段配置表动态表头导出
export const memberextendfieldconfigDynamicExport = (data) => {
    return request("eptpost", '/member/memberExtendFieldConfig/exportDynamic', data)
}
