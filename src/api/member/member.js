import request from "@/common/request"

//会员表分页展示
export const memberPage = (queryForm) => {
    return request("get", "/member/member/page", queryForm);
}
//会员表列表展示
export const memberList = (queryForm) => {
    return request("get", "/member/member/list", queryForm);
}
//会员表详情
export const memberInfo = (id) => {
    return request('get', '/member/member/info', {
        id: id
    })
}
//会员表信息保存
export const memberSave = (data) => {
    return request('post', '/member/member/save', data)
}
//会员表信息修改
export const memberEdit = (data) => {
    return request('post', '/member/member/edit', data)
}
//会员表信息删除
export const memberDel = (id) => {
    return request('post', '/member/member/delete?id=' + id)
}
//会员表批量删除
export const memberDelBatch = (ids) => {
    return request('post', '/member/member/deleteBatch', ids)
}
//会员表动态表头导出
export const memberDynamicExport = (data) => {
    return request("eptpost", '/member/member/exportDynamic', data)
}
