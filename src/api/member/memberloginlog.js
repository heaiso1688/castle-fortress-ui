import request from "@/common/request"

//会员登录日志表分页展示
export const memberloginlogPage = (queryForm) => {
    return request("get", "/member/memberLoginLog/page", queryForm);
}
//会员登录日志表列表展示
export const memberloginlogList = (queryForm) => {
    return request("get", "/member/memberLoginLog/list", queryForm);
}
//会员登录日志表详情
export const memberloginlogInfo = (id) => {
    return request('get', '/member/memberLoginLog/info', {
        id: id
    })
}
//会员登录日志表信息保存
export const memberloginlogSave = (data) => {
    return request('post', '/member/memberLoginLog/save', data)
}
//会员登录日志表信息修改
export const memberloginlogEdit = (data) => {
    return request('post', '/member/memberLoginLog/edit', data)
}
//会员登录日志表信息删除
export const memberloginlogDel = (id) => {
    return request('post', '/member/memberLoginLog/delete?id=' + id)
}
//会员登录日志表批量删除
export const memberloginlogDelBatch = (ids) => {
    return request('post', '/member/memberLoginLog/deleteBatch', ids)
}
//会员登录日志表动态表头导出
export const memberloginlogDynamicExport = (data) => {
    return request("eptpost", '/member/memberLoginLog/exportDynamic', data)
}
