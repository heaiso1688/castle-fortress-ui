import request from "@/common/request"

//帮助中心文章分页展示
export const castlehelparticlePage = (queryForm) => {
    return request("get", "/system/castleHelpArticle/page", queryForm);
}
//帮助中心文章列表展示
export const castlehelparticleList = (queryForm) => {
    return request("get", "/system/castleHelpArticle/list", queryForm);
}
//帮助中心文章详情
export const castlehelparticleInfo = (id) => {
    return request('get', '/system/castleHelpArticle/info', {
        id: id
    })
}
//帮助中心文章信息保存
export const castlehelparticleSave = (data) => {
    return request('post', '/system/castleHelpArticle/save', data)
}
//帮助中心文章信息修改
export const castlehelparticleEdit = (data) => {
    return request('post', '/system/castleHelpArticle/edit', data)
}
//帮助中心文章信息删除
export const castlehelparticleDel = (id) => {
    return request('post', '/system/castleHelpArticle/delete?id=' + id)
}
//帮助中心文章批量删除
export const castlehelparticleDelBatch = (ids) => {
    return request('post', '/system/castleHelpArticle/deleteBatch', ids)
}
//帮助中心文章动态表头导出
export const castlehelparticleDynamicExport = (data) => {
    return request("eptpost", '/system/castleHelpArticle/exportDynamic', data)
}
