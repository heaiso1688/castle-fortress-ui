import request from "@/common/request"

//版本管理分页展示
export const castleversionPage = (queryForm) => {
    return request("get", "/system/castleVersion/page", queryForm);
}
//版本管理列表展示
export const castleversionList = (queryForm) => {
    return request("get", "/system/castleVersion/list", queryForm);
}
//版本管理详情
export const castleversionInfo = (id) => {
    return request('get', '/system/castleVersion/info', {
        id: id
    })
}
//版本管理信息保存
export const castleversionSave = (data) => {
    return request('post', '/system/castleVersion/save', data)
}
//版本管理信息修改
export const castleversionEdit = (data) => {
    return request('post', '/system/castleVersion/edit', data)
}
//版本管理信息删除
export const castleversionDel = (id) => {
    return request('post', '/system/castleVersion/delete?id=' + id)
}
//版本管理批量删除
export const castleversionDelBatch = (ids) => {
    return request('post', '/system/castleVersion/deleteBatch', ids)
}
//版本管理动态表头导出
export const castleversionDynamicExport = (data) => {
    return request("eptpost", '/system/castleVersion/exportDynamic', data)
}
