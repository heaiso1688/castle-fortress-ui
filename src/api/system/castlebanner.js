import request from "@/common/request"

//banner图分页展示
export const castlebannerPage = (queryForm) => {
    return request("get", "/system/castleBanner/page", queryForm);
}
//banner图列表展示
export const castlebannerList = (queryForm) => {
    return request("get", "/system/castleBanner/list", queryForm);
}
//banner图详情
export const castlebannerInfo = (id) => {
    return request('get', '/system/castleBanner/info', {
        id: id
    })
}
//banner图信息保存
export const castlebannerSave = (data) => {
    return request('post', '/system/castleBanner/save', data)
}
//banner图信息修改
export const castlebannerEdit = (data) => {
    return request('post', '/system/castleBanner/edit', data)
}
//banner图信息删除
export const castlebannerDel = (id) => {
    return request('post', '/system/castleBanner/delete?id=' + id)
}
//banner图批量删除
export const castlebannerDelBatch = (ids) => {
    return request('post', '/system/castleBanner/deleteBatch', ids)
}
//banner图动态表头导出
export const castlebannerDynamicExport = (data) => {
    return request("eptpost", '/system/castleBanner/exportDynamic', data)
}
