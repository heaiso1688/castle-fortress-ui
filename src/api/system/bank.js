import request from "@/common/request"

//银行信息分页展示
export const bankPage = (queryForm) => {
    return request("get", "/system/bank/page", queryForm);
}
//银行信息列表展示
export const bankList = (queryForm) => {
    return request("get", "/system/bank/list", queryForm);
}
//银行信息详情
export const bankInfo = (id) => {
    return request('get', '/system/bank/info', {
        id: id
    })
}
//银行信息详情
export const getByNum = (cardNum) => {
    return request('get', '/system/bank/getByNum', {
        cardNum: cardNum
    })
}
//银行信息信息保存
export const bankSave = (data) => {
    return request('post', '/system/bank/save', data)
}
//银行信息信息修改
export const bankEdit = (data) => {
    return request('post', '/system/bank/edit', data)
}
//银行信息信息删除
export const bankDel = (id) => {
    return request('post', '/system/bank/delete?id=' + id)
}
//银行信息批量删除
export const bankDelBatch = (ids) => {
    return request('post', '/system/bank/deleteBatch', ids)
}
//银行信息动态表头导出
export const bankDynamicExport = (data) => {
    return request("eptpost", '/system/bank/exportDynamic', data)
}
