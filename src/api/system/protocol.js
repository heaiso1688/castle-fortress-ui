import request from "@/common/request"

//协议管理分页展示
export const protocolPage = (queryForm) => {
    return request("get", "/system/protocol/page", queryForm);
}
//协议管理列表展示
export const protocolList = (queryForm) => {
    return request("get", "/system/protocol/list", queryForm);
}
//协议管理详情
export const protocolInfo = (id) => {
    return request('get', '/system/protocol/info', {
        id: id
    })
}
//协议管理信息保存
export const protocolSave = (data) => {
    return request('post', '/system/protocol/save', data)
}
//协议管理信息修改
export const protocolEdit = (data) => {
    return request('post', '/system/protocol/edit', data)
}
//协议管理信息删除
export const protocolDel = (id) => {
    return request('post', '/system/protocol/delete?id=' + id)
}
//协议管理批量删除
export const protocolDelBatch = (ids) => {
    return request('post', '/system/protocol/deleteBatch', ids)
}
//协议管理动态表头导出
export const protocolDynamicExport = (data) => {
    return request("eptpost", '/system/protocol/exportDynamic', data)
}
