import request from "@/common/request"

//帮助中心文章类型分页展示
export const castlehelparticletypePage = (queryForm) => {
    return request("get", "/system/castleHelpArticleType/page", queryForm);
}
//帮助中心文章类型列表展示
export const castlehelparticletypeList = (queryForm) => {
    return request("get", "/system/castleHelpArticleType/list", queryForm);
}
//帮助中心文章类型详情
export const castlehelparticletypeInfo = (id) => {
    return request('get', '/system/castleHelpArticleType/info', {
        id: id
    })
}
//帮助中心文章类型信息保存
export const castlehelparticletypeSave = (data) => {
    return request('post', '/system/castleHelpArticleType/save', data)
}
//帮助中心文章类型信息修改
export const castlehelparticletypeEdit = (data) => {
    return request('post', '/system/castleHelpArticleType/edit', data)
}
//帮助中心文章类型信息删除
export const castlehelparticletypeDel = (id) => {
    return request('post', '/system/castleHelpArticleType/delete?id=' + id)
}
//帮助中心文章类型批量删除
export const castlehelparticletypeDelBatch = (ids) => {
    return request('post', '/system/castleHelpArticleType/deleteBatch', ids)
}
//帮助中心文章类型动态表头导出
export const castlehelparticletypeDynamicExport = (data) => {
    return request("eptpost", '/system/castleHelpArticleType/exportDynamic', data)
}

// 获取所有帮助文章类型
export const articleTypeList = (id) => {
    return request('get', '/system/castleHelpArticleType/allList', {
        id: id
    })
}
