import request from "@/common/request"

//oss上传记录分页展示
export const sysossrecordPage = (queryForm) => {
    return request("get", "/system/sysOssRecord/page", queryForm);
}
//oss上传记录列表展示
export const sysossrecordList = (queryForm) => {
    return request("get", "/system/sysOssRecord/list", queryForm);
}
//oss上传记录详情
export const sysossrecordInfo = (id) => {
    return request('get', '/system/sysOssRecord/info', {
        id: id
    })
}
//oss上传记录信息保存
export const sysossrecordSave = (data) => {
    return request('post', '/system/sysOssRecord/save', data)
}
//oss上传记录信息修改
export const sysossrecordEdit = (data) => {
    return request('post', '/system/sysOssRecord/edit', data)
}
//oss上传记录信息删除
export const sysossrecordDel = (id) => {
    return request('post', '/system/sysOssRecord/delete?id=' + id)
}
//oss上传记录批量删除
export const sysossrecordDelBatch = (ids) => {
    return request('post', '/system/sysOssRecord/deleteBatch', ids)
}
//oss上传记录动态表头导出
export const sysossrecordDynamicExport = (data) => {
    return request("eptpost", '/system/sysOssRecord/exportDynamic', data)
}
