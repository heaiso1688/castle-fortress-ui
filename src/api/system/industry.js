import request from "@/common/request"


//获取职位分类三级
export const industryList = (data) => {
    return request('get', '/system/castleSysIndustry/tree', data)
}
