import request from "@/common/request"

//意见反馈分页展示
export const feedbackPage = (queryForm) => {
    return request("get", "/system/feedback/page", queryForm);
}
//意见反馈列表展示
export const feedbackList = (queryForm) => {
    return request("get", "/system/feedback/list", queryForm);
}
//意见反馈详情
export const feedbackInfo = (id) => {
    return request('get', '/system/feedback/info', {
        id: id
    })
}
//意见反馈信息保存
export const feedbackSave = (data) => {
    return request('post', '/system/feedback/save', data)
}
//意见反馈信息修改
export const feedbackEdit = (data) => {
    return request('post', '/system/feedback/edit', data)
}
//意见反馈信息删除
export const feedbackDel = (id) => {
    return request('post', '/system/feedback/delete?id=' + id)
}
//意见反馈批量删除
export const feedbackDelBatch = (ids) => {
    return request('post', '/system/feedback/deleteBatch', ids)
}
//意见反馈动态表头导出
export const feedbackDynamicExport = (data) => {
    return request("eptpost", '/system/feedback/exportDynamic', data)
}
