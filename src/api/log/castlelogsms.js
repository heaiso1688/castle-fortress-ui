import request from "@/common/request"

//短信发送记录分页展示
export const castlelogsmsPage = (queryForm) => {
    return request("get", "/log/castleLogSms/page", queryForm);
}
//短信发送记录列表展示
export const castlelogsmsList = (queryForm) => {
    return request("get", "/log/castleLogSms/list", queryForm);
}
//短信发送记录详情
export const castlelogsmsInfo = (id) => {
    return request('get', '/log/castleLogSms/info', {
        id: id
    })
}
//短信发送记录信息保存
export const castlelogsmsSave = (data) => {
    return request('post', '/log/castleLogSms/save', data)
}
//短信发送记录信息修改
export const castlelogsmsEdit = (data) => {
    return request('post', '/log/castleLogSms/edit', data)
}
//短信发送记录信息删除
export const castlelogsmsDel = (id) => {
    return request('post', '/log/castleLogSms/delete?id=' + id)
}
//短信发送记录批量删除
export const castlelogsmsDelBatch = (ids) => {
    return request('post', '/log/castleLogSms/deleteBatch', ids)
}
//短信发送记录动态表头导出
export const castlelogsmsDynamicExport = (data) => {
    return request("eptpost", '/log/castleLogSms/exportDynamic', data)
}
