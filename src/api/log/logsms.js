import request from "@/common/request"

//短信发送记录分页展示
export const logsmsPage = (queryForm) => {
    return request("get", "/log/logSms/page", queryForm);
}
//短信发送记录列表展示
export const logsmsList = (queryForm) => {
    return request("get", "/log/logSms/list", queryForm);
}
//短信发送记录详情
export const logsmsInfo = (id) => {
    return request('get', '/log/logSms/info', {
        id: id
    })
}
//短信发送记录信息保存
export const logsmsSave = (data) => {
    return request('post', '/log/logSms/save', data)
}
//短信发送记录信息修改
export const logsmsEdit = (data) => {
    return request('post', '/log/logSms/edit', data)
}
//短信发送记录信息删除
export const logsmsDel = (id) => {
    return request('post', '/log/logSms/delete?id=' + id)
}
//短信发送记录批量删除
export const logsmsDelBatch = (ids) => {
    return request('post', '/log/logSms/deleteBatch', ids)
}
//短信发送记录动态表头导出
export const logsmsDynamicExport = (data) => {
    return request("eptpost", '/log/logSms/exportDynamic', data)
}
