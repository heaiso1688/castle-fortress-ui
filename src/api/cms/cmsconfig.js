import request from "@/common/request"

//cms通用配置表分页展示
export const cmsconfigPage = (queryForm) => {
    return request("get", "/cms/cmsConfig/page", queryForm);
}
//cms通用配置表列表展示
export const cmsconfigList = (queryForm) => {
    return request("get", "/cms/cmsConfig/list", queryForm);
}
//cms通用配置表详情
export const cmsconfigInfo = (id) => {
    return request('get', '/cms/cmsConfig/info', {
        id: id
    })
}
//cms通用配置表信息保存
export const cmsconfigSave = (data) => {
    return request('post', '/cms/cmsConfig/save', data)
}
//cms通用配置表信息修改
export const cmsconfigEdit = (data) => {
    return request('post', '/cms/cmsConfig/edit', data)
}
//cms通用配置表信息删除
export const cmsconfigDel = (id) => {
    return request('post', '/cms/cmsConfig/delete?id=' + id)
}
//cms通用配置表批量删除
export const cmsconfigDelBatch = (ids) => {
    return request('post', '/cms/cmsConfig/deleteBatch', ids)
}
//cms通用配置表动态表头导出
export const cmsconfigDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsConfig/exportDynamic', data)
}
//cms通用配置信息修改
export const cmsconfigListEdit = (data) => {
    return request('post', '/cms/cmsConfig/listedit', data)
}
