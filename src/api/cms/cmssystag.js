import request from "@/common/request"

//cms系统预设标签表分页展示
export const cmssystagPage = (queryForm) => {
    return request("get", "/cms/cmsSysTag/page", queryForm);
}
//cms系统预设标签表列表展示
export const cmssystagList = (queryForm) => {
    return request("get", "/cms/cmsSysTag/list", queryForm);
}
//cms系统预设标签表详情
export const cmssystagInfo = (id) => {
    return request('get', '/cms/cmsSysTag/info', {
        id: id
    })
}
//cms系统预设标签表信息保存
export const cmssystagSave = (data) => {
    return request('post', '/cms/cmsSysTag/save', data)
}
//cms系统预设标签表信息修改
export const cmssystagEdit = (data) => {
    return request('post', '/cms/cmsSysTag/edit', data)
}
//cms系统预设标签表信息删除
export const cmssystagDel = (id) => {
    return request('post', '/cms/cmsSysTag/delete?id=' + id)
}
//cms系统预设标签表批量删除
export const cmssystagDelBatch = (ids) => {
    return request('post', '/cms/cmsSysTag/deleteBatch', ids)
}
//cms系统预设标签表动态表头导出
export const cmssystagDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsSysTag/exportDynamic', data)
}
