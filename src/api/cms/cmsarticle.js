import request from "@/common/request"

//cms内容管理表左树数据展示
export const leftTreeList = queryForm => {
	return request("get", "/cms/cmsChannel/tree", queryForm)
}
//cms内容管理表左树数据展示
export const leftTreeAuthTree = queryForm => {
	return request("get", "/cms/cmsChannel/authTree", queryForm)
}

//cms内容管理表分页展示
export const cmsarticlePage = queryForm => {
	return request("get", "/cms/cmsArticle/page", queryForm)
}
//cms内容管理表列表展示
export const cmsarticleList = queryForm => {
	return request("get", "/cms/cmsArticle/list", queryForm)
}
//cms内容管理表详情
export const cmsarticleInfo = id => {
	return request("get", "/cms/cmsArticle/info", {
		id: id,
	})
}
//cms内容管理表信息保存
export const cmsarticleSave = data => {
	return request("post", "/cms/cmsArticle/save", data)
}
//cms内容管理表信息修改
export const cmsarticleEdit = data => {
	return request("post", "/cms/cmsArticle/edit", data)
}
//cms内容管理表信息删除
export const cmsarticleDel = id => {
	return request("post", "/cms/cmsArticle/delete?id=" + id)
}
//cms内容管理表批量删除
export const cmsarticleDelBatch = ids => {
	return request("post", "/cms/cmsArticle/deleteBatch", ids)
}
//cms内容管理表动态表头导出
export const cmsarticleDynamicExport = data => {
	return request("eptpost", "/cms/cmsArticle/exportDynamic", data)
}

//cms审核文章分页展示
export const cmsarticleAuditPage = queryForm => {
	return request("get", "/cms/cmsArticle/pageaudit", queryForm)
}
//cms审核
export const cmsarticleAudit = data => {
	return request("post", "/cms/cmsArticle/audit", data)
}
