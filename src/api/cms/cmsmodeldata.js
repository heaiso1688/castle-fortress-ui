import request from "@/common/request"

//cms模型数据表单信息
export const cmsformInfo = (queryForm) => {
    return request("get", "/cms/modelData/formInfo", queryForm);
}
