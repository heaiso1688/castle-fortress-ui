import request from "@/common/request"

//cms栏目/导航菜单配置表分页展示
export const cmschannelTree = queryForm => {
	return request("get", "/cms/cmsChannel/tree", queryForm)
}
//cms栏目/导航菜单配置表权限可见分页展示
export const cmschannelAuthTree = queryForm => {
	return request("get", "/cms/cmsChannel/authTree", queryForm)
}
//cms栏目/导航菜单配置表列表展示
export const cmschannelList = queryForm => {
	return request("get", "/cms/cmsChannel/list", queryForm)
}
//cms栏目/导航菜单配置表详情
export const cmschannelInfo = id => {
	return request("get", "/cms/cmsChannel/info", {
		id: id,
	})
}
//cms栏目/导航菜单配置表信息保存
export const cmschannelSave = data => {
	return request("post", "/cms/cmsChannel/save", data)
}
//cms栏目/导航菜单配置表信息修改
export const cmschannelEdit = data => {
	return request("post", "/cms/cmsChannel/edit", data)
}
//cms栏目/导航菜单配置表信息删除
export const cmschannelDel = id => {
	return request("post", "/cms/cmsChannel/delete?id=" + id)
}
//cms栏目/导航菜单配置表批量删除
export const cmschannelDelBatch = ids => {
	return request("post", "/cms/cmsChannel/deleteBatch", ids)
}
//cms栏目/导航菜单配置表动态表头导出
export const cmschannelDynamicExport = data => {
	return request("eptpost", "/cms/cmsChannel/exportDynamic", data)
}
//cms栏目/导航菜单模型的扩展字段
export const cmschannelCols = id => {
	return request("get", "/cms/cmsChannel/cols", {
		id: id,
	})
}
