import request from "@/common/request"

//cms模型配置表分页展示
export const cmsmodelPage = (queryForm) => {
    return request("get", "/cms/cmsModel/page", queryForm);
}
//cms模型配置表列表展示
export const cmsmodelList = (queryForm) => {
    return request("get", "/cms/cmsModel/list", queryForm);
}
//cms模型配置表详情
export const cmsmodelInfo = (id) => {
    return request('get', '/cms/cmsModel/info', {
        id: id
    })
}
//cms模型系统预设字段
export const cmsmodelFields = () => {
    return request('get', '/cms/cmsModel/sysCols')
}
//cms模型配置表信息保存
export const cmsmodelSave = (data) => {
    return request('post', '/cms/cmsModel/save', data)
}
//cms模型配置表信息修改
export const cmsmodelEdit = (data) => {
    return request('post', '/cms/cmsModel/edit', data)
}
//cms模型配置表信息删除
export const cmsmodelDel = (id) => {
    return request('post', '/cms/cmsModel/delete?id=' + id)
}
//cms模型配置表批量删除
export const cmsmodelDelBatch = (ids) => {
    return request('post', '/cms/cmsModel/deleteBatch', ids)
}
//cms模型配置表动态表头导出
export const cmsmodelDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsModel/exportDynamic', data)
}
