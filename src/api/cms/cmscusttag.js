import request from "@/common/request"

//cms用户自定义标签表分页展示
export const cmscusttagPage = (queryForm) => {
    return request("get", "/cms/cmsCustTag/page", queryForm);
}
//cms用户自定义标签表列表展示
export const cmscusttagList = (queryForm) => {
    return request("get", "/cms/cmsCustTag/list", queryForm);
}
//cms用户自定义标签表详情
export const cmscusttagInfo = (id) => {
    return request('get', '/cms/cmsCustTag/info', {
        id: id
    })
}
//cms用户自定义标签表信息保存
export const cmscusttagSave = (data) => {
    return request('post', '/cms/cmsCustTag/save', data)
}
//cms用户自定义标签表信息修改
export const cmscusttagEdit = (data) => {
    return request('post', '/cms/cmsCustTag/edit', data)
}
//cms用户自定义标签表信息删除
export const cmscusttagDel = (id) => {
    return request('post', '/cms/cmsCustTag/delete?id=' + id)
}
//cms用户自定义标签表批量删除
export const cmscusttagDelBatch = (ids) => {
    return request('post', '/cms/cmsCustTag/deleteBatch', ids)
}
//cms用户自定义标签表动态表头导出
export const cmscusttagDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsCustTag/exportDynamic', data)
}
