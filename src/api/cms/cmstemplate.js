import request from "@/common/request"

//cms模板管理表分页展示
export const cmstemplatePage = (queryForm) => {
    return request("get", "/cms/cmsTemplate/page", queryForm);
}
//cms模板管理表列表展示
export const cmstemplateList = (queryForm) => {
    return request("get", "/cms/cmsTemplate/list", queryForm);
}

//cms模板状态管理
export const cmstemplateEdit = (data) => {
    return request('post', '/cms/cmsTemplate/edit', data)
}
//cms模板管理表动态表头导出
export const cmstemplateDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsTemplate/exportDynamic', data)
}
//获取文件内容
export const getContents = (data) => {
    return request('post', '/cms/cmsTemplate/contents', data)
}
//cms模板内容保存
export const cmstemplateSave = (data) => {
    return request('post', '/cms/cmsTemplate/save', data)
}
//cms模板文件删除
export const cmstemplateDel = (data) => {
    return request('post', '/cms/cmsTemplate/delete', data)
}
//cms模板文件过滤 支持 cover 单页 list 列表 article 文章
export const cmstemplateFilter = (type) => {
    return request('get', '/cms/cmsTemplate/filter', {
        type: type
    })
}
