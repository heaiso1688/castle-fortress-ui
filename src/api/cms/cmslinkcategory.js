import request from "@/common/request"

//cms友链分类管理表分页展示
export const cmslinkcategoryPage = (queryForm) => {
    return request("get", "/cms/cmsLinkCategory/page", queryForm);
}
//cms友链分类管理表列表展示
export const cmslinkcategoryList = (queryForm) => {
    return request("get", "/cms/cmsLinkCategory/list", queryForm);
}
//cms友链分类管理表详情
export const cmslinkcategoryInfo = (id) => {
    return request('get', '/cms/cmsLinkCategory/info', {
        id: id
    })
}
//cms友链分类管理表信息保存
export const cmslinkcategorySave = (data) => {
    return request('post', '/cms/cmsLinkCategory/save', data)
}
//cms友链分类管理表信息修改
export const cmslinkcategoryEdit = (data) => {
    return request('post', '/cms/cmsLinkCategory/edit', data)
}
//cms友链分类管理表信息删除
export const cmslinkcategoryDel = (id) => {
    return request('post', '/cms/cmsLinkCategory/delete?id=' + id)
}
//cms友链分类管理表批量删除
export const cmslinkcategoryDelBatch = (ids) => {
    return request('post', '/cms/cmsLinkCategory/deleteBatch', ids)
}
//cms友链分类管理表动态表头导出
export const cmslinkcategoryDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsLinkCategory/exportDynamic', data)
}
