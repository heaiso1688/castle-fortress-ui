import request from "@/common/request"

//cms专题配置表分页展示
export const cmssubjectPage = (queryForm) => {
    return request("get", "/cms/cmsSubject/page", queryForm);
}
//cms专题配置表列表展示
export const cmssubjectList = (queryForm) => {
    return request("get", "/cms/cmsSubject/list", queryForm);
}
//cms专题配置表详情
export const cmssubjectInfo = (id) => {
    return request('get', '/cms/cmsSubject/info', {
        id: id
    })
}
//cms专题配置表信息保存
export const cmssubjectSave = (data) => {
    return request('post', '/cms/cmsSubject/save', data)
}
//cms专题配置表信息修改
export const cmssubjectEdit = (data) => {
    return request('post', '/cms/cmsSubject/edit', data)
}
//cms专题配置表信息删除
export const cmssubjectDel = (id) => {
    return request('post', '/cms/cmsSubject/delete?id=' + id)
}
//cms专题配置表批量删除
export const cmssubjectDelBatch = (ids) => {
    return request('post', '/cms/cmsSubject/deleteBatch', ids)
}
//cms专题配置表动态表头导出
export const cmssubjectDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsSubject/exportDynamic', data)
}
