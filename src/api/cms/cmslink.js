import request from "@/common/request"

//cms友链管理表分页展示
export const cmslinkPage = (queryForm) => {
    return request("get", "/cms/cmsLink/page", queryForm);
}
//cms友链管理表列表展示
export const cmslinkList = (queryForm) => {
    return request("get", "/cms/cmsLink/list", queryForm);
}
//cms友链管理表详情
export const cmslinkInfo = (id) => {
    return request('get', '/cms/cmsLink/info', {
        id: id
    })
}
//cms友链管理表信息保存
export const cmslinkSave = (data) => {
    return request('post', '/cms/cmsLink/save', data)
}
//cms友链管理表信息修改
export const cmslinkEdit = (data) => {
    return request('post', '/cms/cmsLink/edit', data)
}
//cms友链管理表信息删除
export const cmslinkDel = (id) => {
    return request('post', '/cms/cmsLink/delete?id=' + id)
}
//cms友链管理表批量删除
export const cmslinkDelBatch = (ids) => {
    return request('post', '/cms/cmsLink/deleteBatch', ids)
}
//cms友链管理表动态表头导出
export const cmslinkDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsLink/exportDynamic', data)
}
