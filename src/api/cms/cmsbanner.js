import request from "@/common/request"

//cms轮播图管理表分页展示
export const cmsbannerPage = (queryForm) => {
    return request("get", "/cms/cmsBanner/page", queryForm);
}
//cms轮播图管理表列表展示
export const cmsbannerList = (queryForm) => {
    return request("get", "/cms/cmsBanner/list", queryForm);
}
//cms轮播图管理表详情
export const cmsbannerInfo = (id) => {
    return request('get', '/cms/cmsBanner/info', {
        id: id
    })
}
//cms轮播图管理表信息保存
export const cmsbannerSave = (data) => {
    return request('post', '/cms/cmsBanner/save', data)
}
//cms轮播图管理表信息修改
export const cmsbannerEdit = (data) => {
    return request('post', '/cms/cmsBanner/edit', data)
}
//cms轮播图管理表信息删除
export const cmsbannerDel = (id) => {
    return request('post', '/cms/cmsBanner/delete?id=' + id)
}
//cms轮播图管理表批量删除
export const cmsbannerDelBatch = (ids) => {
    return request('post', '/cms/cmsBanner/deleteBatch', ids)
}
//cms轮播图管理表动态表头导出
export const cmsbannerDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsBanner/exportDynamic', data)
}
