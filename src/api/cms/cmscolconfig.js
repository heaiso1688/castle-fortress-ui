import request from "@/common/request"

//cms模型字段配置表分页展示
export const cmscolconfigPage = (queryForm) => {
    return request("get", "/cms/cmsColConfig/page", queryForm);
}
//cms模型字段配置表列表展示
export const cmscolconfigList = (queryForm) => {
    return request("get", "/cms/cmsColConfig/list", queryForm);
}
//cms模型字段配置表详情
export const cmscolconfigInfo = (id) => {
    return request('get', '/cms/cmsColConfig/info', {
        id: id
    })
}
//cms模型字段配置表信息保存
export const cmscolconfigSave = (data) => {
    return request('post', '/cms/cmsColConfig/save', data)
}
//cms模型字段配置表信息修改
export const cmscolconfigEdit = (data) => {
    return request('post', '/cms/cmsColConfig/edit', data)
}
//cms模型字段配置表信息删除
export const cmscolconfigDel = (id) => {
    return request('post', '/cms/cmsColConfig/delete?id=' + id)
}
//cms模型字段配置表批量删除
export const cmscolconfigDelBatch = (ids) => {
    return request('post', '/cms/cmsColConfig/deleteBatch', ids)
}
//cms模型字段配置表动态表头导出
export const cmscolconfigDynamicExport = (data) => {
    return request("eptpost", '/cms/cmsColConfig/exportDynamic', data)
}
