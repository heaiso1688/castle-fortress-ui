import request from "@/common/request"

//兑换券分页展示
export const exchangevoucherPage = (queryForm) => {
    return request("get", "/exchange/exchangeVoucher/page", queryForm);
}
//兑换券列表展示
export const exchangevoucherList = (queryForm) => {
    return request("get", "/exchange/exchangeVoucher/list", queryForm);
}
//兑换券详情
export const exchangevoucherInfo = (id) => {
    return request('get', '/exchange/exchangeVoucher/info', {
        id: id
    })
}
//兑换券信息保存
export const exchangevoucherSave = (data) => {
    return request('post', '/exchange/exchangeVoucher/save', data)
}
//
export const exchangevoucherGenerate = (data) => {
    return request('post', '/exchange/exchangeVoucher/generate', data)
}
//兑换券信息修改
export const exchangevoucherEdit = (data) => {
    return request('post', '/exchange/exchangeVoucher/edit', data)
}
//兑换券信息删除
export const exchangevoucherDel = (id) => {
    return request('post', '/exchange/exchangeVoucher/delete?id=' + id)
}
//兑换券批量删除
export const exchangevoucherDelBatch = (ids) => {
    return request('post', '/exchange/exchangeVoucher/deleteBatch', ids)
}
//兑换券动态表头导出
export const exchangevoucherDynamicExport = (data) => {
    return request("eptpost", '/exchange/exchangeVoucher/exportDynamic', data)
}
export const exchangeVouCherDownload = (data) => {
    return request("eptpost", 'exchange/exchangeVoucher/download', data);
}
