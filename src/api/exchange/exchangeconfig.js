import request from "@/common/request"

//兑换中心设置分页展示
export const exchangeconfigPage = (queryForm) => {
    return request("get", "/exchange/exchangeConfig/page", queryForm);
}
//兑换中心设置列表展示
export const exchangeconfigList = (queryForm) => {
    return request("get", "/exchange/exchangeConfig/list", queryForm);
}
//兑换中心设置详情
export const exchangeconfigInfo = (id) => {
    return request('get', '/exchange/exchangeConfig/info', {
        id: id
    })
}
//兑换中心设置信息保存
export const exchangeconfigSave = (data) => {
    return request('post', '/exchange/exchangeConfig/save', data)
}
//兑换中心设置信息修改
export const exchangeconfigEdit = (data) => {
    return request('post', '/exchange/exchangeConfig/edit', data)
}
//兑换中心设置信息删除
export const exchangeconfigDel = (id) => {
    return request('post', '/exchange/exchangeConfig/delete?id=' + id)
}
//兑换中心设置批量删除
export const exchangeconfigDelBatch = (ids) => {
    return request('post', '/exchange/exchangeConfig/deleteBatch', ids)
}
//兑换中心设置动态表头导出
export const exchangeconfigDynamicExport = (data) => {
    return request("eptpost", '/exchange/exchangeConfig/exportDynamic', data)
}
