import FortressMap from './fortress-map'

FortressMap.install = function (Vue) {
    Vue.component(FortressMap.name, FortressMap)
}

export default FortressMap
