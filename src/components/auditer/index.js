import VAuditer from "./index.vue"

VAuditer.install = function(Vue) {
	Vue.component(VAuditer.name, VAuditer)
}

export default VAuditer
