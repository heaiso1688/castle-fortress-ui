import FortressCopy from './fortress-copy'

FortressCopy.install = function (Vue) {
    Vue.component(FortressCopy.name, FortressCopy)
}

export default FortressCopy
