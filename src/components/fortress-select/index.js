import FortressSelect from './fortress-select'

FortressSelect.install = function (Vue) {
    Vue.component(FortressSelect.name, FortressSelect)
}

export default FortressSelect
