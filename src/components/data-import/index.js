import DataImport from "./index.vue"

DataImport.install = function(Vue) {
	Vue.component(DataImport.name, DataImport)
}

export default DataImport
