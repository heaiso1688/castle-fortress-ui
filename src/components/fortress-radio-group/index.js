import FortressRadioGroup from './fortress-radio-group'

FortressRadioGroup.install = function (Vue) {
    Vue.component(FortressRadioGroup.name, FortressRadioGroup)
}

export default FortressRadioGroup
