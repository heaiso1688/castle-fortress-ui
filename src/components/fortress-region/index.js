import FortressRegion from './fortress-region'

FortressRegion.install = function (Vue) {
    Vue.component(FortressRegion.name, FortressRegion)
}

export default FortressRegion
