import FortressEditor from './fortress-editor'

FortressEditor.install = function (Vue) {
    Vue.component(FortressEditor.name, FortressEditor)
}

export default FortressEditor
