import FortressSearch from './fortress-search'

FortressSearch.install = function(Vue) {
    Vue.component(FortressSearch.name, FortressSearch)
}

export default FortressSearch