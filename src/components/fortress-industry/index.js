import FortressIndustry from './fortress-industry'

FortressIndustry.install = function (Vue) {
    Vue.component(FortressIndustry.name, FortressIndustry)
}

export default FortressIndustry
