import CreateRandom from "./index.vue"

CreateRandom.install = function(Vue) {
	Vue.component(CreateRandom.name, CreateRandom)
}

export default CreateRandom
