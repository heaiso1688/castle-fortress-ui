import FortressUpload from './fortress-upload'

FortressUpload.install = function (Vue) {
    Vue.component(FortressUpload.name, FortressUpload)
}

export default FortressUpload
